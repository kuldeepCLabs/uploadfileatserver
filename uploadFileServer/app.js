process.env.NODE_ENV='localDevelopment';
config=require('config');
dbConnection = require('./routes/dbConnection');
constant=require('./routes/constant');


var express = require('express');
var path=require('path');
var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();
var http=require('http');
var bodyParser = require('body-parser');


var routes = require('./routes/index');
var fileupload = require('./routes/fileUploadServer');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.set('json spaces',1);

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.get('/test', routes);
app.post('/fileupload',multipartMiddleware);
app.post('/fileupload', fileupload);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});


http.createServer(app).listen(config.get('PORT'),function(req,res){
    console.log("express server listen at the port : "+config.get('PORT'));
});


module.exports = app;

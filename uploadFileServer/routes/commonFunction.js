/**
 * Created by ABC on 3/4/2015.
 */
var sendResponse=require('./sendResponse');
var math=require('math');
var fs=require('fs');
var AWS=require('aws-sdk');
var s3=config.get('AWS_SETTINGS');

// function used to generate the random file name
exports.generateRandomNumber=function() {
    var timestamp = new Date().getTime().toString();
    var str = '';
    var chars = "abcdefghijkulmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123456789";
    var size=chars.length;

    for (var i = 0; i < 4; i++) {
        var randno = math.floor(math.random() * size);
        str = chars[randno] + str;
    }
    return str+timestamp;
}

//check the blank value at any parameter
exports.checkBlank = function (res, manValues, callback) {

    var checkBlankData = checkBlank(manValues);

    if (checkBlankData) {
        sendResponse.parameterMissingError(res);
    }
    else {
        callback(null);
    }
}

function checkBlank(arr) {

    var arrlength = arr.length;

    for (var i = 0; i < arrlength; i++) {
        if (arr[i] == ''||arr[i] == undefined||arr[i] == '(null)') {
            return 1;
            break;
        }
    }
    return 0;
}

//upload the image at the server s3 bucket
exports.uploadImageToS3Bucket=function(file,folder,callback){
    var filename=file.name;
    var path=file.path;
    var mimetype=file.type;

    fs.readFile(path,function(err,file_buffer){
        if(err){
            return callback(0);
        }

        AWS.config.update({accessKeyId: s3.accessId, secretAccessKey: s3.secretKey});
        var s3bucket = new AWS.S3();
        var params = {Bucket: s3.bucketName, Key: folder + '/' + filename, Body: file_buffer, ACL: s3.acl1, ContentType: mimetype};

        s3bucket.putObject(params, function(err, data) {
            if (err){
                return callback(0);
            }
            else{

                return callback(filename);

            }
        });

    });
}

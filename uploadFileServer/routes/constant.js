/**
 * The node-module to hold the constants for the server
 */


function define(obj, name, value) {
    Object.defineProperty(obj, name, {
        value:        value,
        enumerable:   true,
        writable:     false,
        configurable: false
    });
}


exports.responseStatus = {};

define(exports.responseStatus, "PARAMETER_MISSING", 100);
define(exports.responseStatus, "INVALID_ACCESS_TOKEN", 101);
define(exports.responseStatus, "ERROR_IN_EXECUTION", 102);
define(exports.responseStatus, "SHOW_ERROR_MESSAGE", 103);
define(exports.responseStatus, "SHOW_MESSAGE", 104);
define(exports.responseStatus, "SHOW_DATA", 105);

exports.responseMessage = {}; define(exports.responseMessage, "PARAMETER_MISSING", "Some Parameters Missing");
define(exports.responseMessage, "INVALID_ACCESS_TOKEN", "Please logout again, Invalid access.");
define(exports.responseMessage, "ERROR_IN_EXECUTION", "Some error occurred. Please try again.");
define(exports.responseMessage, "SHOW_ERROR_MESSAGE", "Some error occurred. Please try again.");
define(exports.responseMessage, "SHOW_MESSAGE", "Hi there!");
define(exports.responseMessage, "SHOW_DATA", "Successfully login the user");
define(exports.responseMessage, "UPLOAD_IMAGE_ERR", "does not upload the image generate the any error");
define(exports.responseMessage, "UPLOAD_IMAGE", "Successfully update the image");
define(exports.responseMessage, "READ_FILE", "does not read the file");

exports.deviceType = {};
define(exports.deviceType, "ANDROID",   0);
define(exports.deviceType, "iOS",       1);
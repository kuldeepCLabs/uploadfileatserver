var express = require('express');
var router = express.Router();
var func=require('./commonFunction');
var sendResponse=require('./sendResponse');
var fs=require('fs');
/* GET users listing. */

//file upload at the server
router.post('/fileupload',function(req,res){
    var randomFileName=func.generateRandomNumber();
    var imagevalue=req.files.file.name;

    console.log(imagevalue);
    func.checkBlank(res,[imagevalue],function(callback){
        if(callback==null)
        {
            req.files.file.name= randomFileName;
            var imageFile=req.files.file;
          //  var imageFile=imageFile+randomFileName;
            console.log(req.files.file.name);
            func.uploadImageToS3Bucket(imageFile,'images',function(result_image){

                if(result_image===0){
                    sendResponse.sendErrorMessage(constant.responseMessage.UPLOAD_IMAGE_ERR,res);
                }

                else{
                    var s3_path = 'http://internsclabs.s3.amazonaws.com/images/'
                    var path = s3_path + result_image;

                    var sql ="insert into `uploadfile` (`file_name`) values(?)";

                    dbConnection.Query(res,sql, [path], function (result5) {
                        if(result5.length==0){
                            sendResponse.sendErrorMessage(constant.responseMessage.UPLOAD_IMAGE_ERR,res);
                        }else{
                            sendResponse.successStatusMsg(constant.responseMessage.UPLOAD_IMAGE,res);
                        }
                    });
                }
            });
        }
    });
});

module.exports = router;
